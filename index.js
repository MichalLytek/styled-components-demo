import React from "react";
import ReactDOM from "react-dom";
import styled from "styled-components";

import "./global.css";
import Buttons from "./buttons";

const AppWrapper = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
`;

const AppTitle = styled.h1`
  margin: 12px 6px;
`;

const ButtonsWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  flex-grow: 1;
  & > * {
    margin: 12px;
  }
`;

const App = () => (
  <AppWrapper>
    <AppTitle>styled-components demo</AppTitle>
    <ButtonsWrapper>
      <Buttons />
    </ButtonsWrapper>
  </AppWrapper>
);

ReactDOM.render(<App />, document.getElementById("container"));
