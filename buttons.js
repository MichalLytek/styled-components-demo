import React, { Fragment } from "react";
import styled from "styled-components";
import color from "color";

// utils.js
const getColor = props => {
  let buttonColor = props.primary ? "#007bff" : props.danger ? "#F44" : "#999";
  if (props.darken) {
    buttonColor = color(buttonColor)
      .darken(0.5)
      .hex();
  }
  return buttonColor;
};

const Button = styled.button`
  border: 3px solid ${getColor};
  border-radius: 8px;
  color: ${getColor};
  font-weight: bold;
  font-size: 16px;
  background-color: transparent;
  padding: 8px 18px;
  cursor: pointer;
  transition: transform 0.1s ease-in-out;

  &:hover {
    transform: scale(1.2);
  }
`;

const FullButton = Button.extend`
  color: white;
  background-color: ${getColor};
`;

const Link = Button.withComponent("a");

export default () => (
  <Fragment>
    <Button>Normal</Button>
    <Button primary>Primary 1</Button>
    <Button primary>Primary 2</Button>
    <Button danger>Danger</Button>
    <FullButton primary>Full</FullButton>
    <FullButton primary darken>
      Darken
    </FullButton>
    <Link href="https://gorrion.io/">Link</Link>
  </Fragment>
);
